// const btn = document.createElement('button');
// btn.innerText = 'Click me';
// btn.setAttribute('id', 'btn-click');

// const section = document.createElement('section');
// section.setAttribute('id', 'content');

// btn.addEventListener('click', () => {
//     const paragraph = document.createElement('p');
//     paragraph.innerText = 'New paragraph';
//     section.append(paragraph);
// })

// document.body.append(btn);
// document.body.append(section);


const btn1 = document.createElement('button');
btn1.setAttribute('id', 'btn-input-create');
btn1.innerText = 'Click on me';
const section = document.querySelector('section');

btn1.style.width = '30%'

btn1.addEventListener('click', () => {
    const input = document.createElement('input');
    input.setAttribute('type', 'text');
    input.setAttribute('name', 'inputText');
    input.setAttribute('placeholder', 'You can write some text');
    section.append(input);
}, { once: true });

section.append(btn1);